#include <Arduino.h> // Arduino 1.0

/**
 * Move 1 motor in some direction
 **/
void moveMotor(int motor, int direction) {
	if (direction == 1) {
		Serial.print("FOWRWARD: ");
		Serial.println(motor);
	} else if (direction == 2) {
		Serial.print("BACK: ");
		Serial.println(motor);
	}
}

/**
 * Move robot in direction
 **/
void move(int direction) {
	if (direction == 1) {
		Serial.println("FOWRWARD");
	} else if (direction == 2) {
		Serial.println("BACK");
	} else if (direction == 3) {
		Serial.println("LEFT");
	} else if (direction == 4) {
		Serial.println("RIGHT");
	}
}
