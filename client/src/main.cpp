#include <EtherCard.h>
#include <PS2Keyboard.h>

/*
Keyboard setup
  2. .1
4.     .3
 6. | .5
1 - data
3 - GND
4 - VCC
5 - clock
*/
const int DataPin = 8;
const int IRQpin =  2;
PS2Keyboard keyboard;

/*
LAN setup
*/
static byte mymac[] = { 0x1A,0x2B,0x3C,0x4D,0x5E,0x6F };
byte Ethernet::buffer[700];
static uint32_t timer;

#define STATIC 1  // set to 1 to disable DHCP (adjust myip/gwip values below)

#if STATIC
// ethernet interface ip address
static byte myip[] = { 192,168,0,2 };
static byte servip[]= {192,168,0,200 };
// gateway ip address
static byte gwip[] = { 192,168,0,1 };
#endif

char sendbuff[10];

const char website[] PROGMEM = "192.168.0.200";
const int dstPort PROGMEM = 1337;

const int srcPort PROGMEM = 4321;

void setup () {
  Serial.begin(9600);

	keyboard.begin(DataPin, IRQpin, PS2Keymap_US);

  if (ether.begin(sizeof Ethernet::buffer, mymac) == 0)
    Serial.println( "Failed to access Ethernet controller");

	#if STATIC
  ether.staticSetup(myip, gwip);
	#else
  if (!ether.dhcpSetup())
    Serial.println(F("DHCP failed"));
	#endif

	ether.printIp("IP:  ", ether.myip);
	ether.printIp("GW:  ", ether.gwip);
	ether.printIp("DNS: ", ether.dnsip);

//  if (!ether.dnsLookup(website))
//    Serial.println("DNS failed");
  ether.hisip[0]=192;
  ether.hisip[1]=168;
  ether.hisip[2]=0;
  ether.hisip[3]=200;

  ether.printIp("SRV: ", ether.hisip);
}

//char textToSend[] = "test 123";

void loop () {
	if (millis() > timer) {
		char _key = '0';
		if (keyboard.available()) {
		 _key = keyboard.read();
		 Serial.print(_key);
		}
		sprintf(sendbuff, "%c", _key);
		timer = millis() + 50;
		// Serial.print(F("Send:"));

		// Serial.println(sendbuff);

		//static void sendUdp (char *data,uint8_t len,uint16_t sport, uint8_t *dip, uint16_t dport);
		ether.sendUdp(sendbuff, sizeof(sendbuff), srcPort, ether.hisip, dstPort );
	}
}
